<?php

namespace Database\Seeders;

use App\Models\Hall;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class HallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hall = Hall::create([
            "name" => "Meeting Hall",
            "description" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet totam iste laborum necessitatibus sequi reprehenderit aspernatur labore est officiis itaque.",
            "occupancy" => "15",
            "floor_id" => "1",
            "price" => "3000",
            "image" => "images/halls/Meeting_hall.jpg",
        ]);
        $hall->amenities()->sync([1, 2, 5]);
        $hall = Hall::create([
            "name" => "Wedding or Party Hall",
            "description" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet totam iste laborum necessitatibus sequi reprehenderit aspernatur labore est officiis itaque.",
            "occupancy" => "300",
            "floor_id" => "1",
            "price" => "25000",
            "image" => "images/halls/Wedding_hall.jpg",
        ]);
        $hall->amenities()->sync([1, 2, 5]);
        $hall = Hall::create([
            "name" => "Banquet Hall",
            "description" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet totam iste laborum necessitatibus sequi reprehenderit aspernatur labore est officiis itaque.",
            "occupancy" => "150",
            "floor_id" => "1",
            "price" => "30000",
            "image" => "images/halls/Banquet_hall.jpg",
        ]);
        $hall->amenities()->sync([1, 2, 5]);
    }
}
