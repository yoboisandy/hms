<?php

namespace Database\Seeders;

use App\Models\Room;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Room::create([
            'room_no' => '201',
            'floor_id' => '1',
            'roomtype_id' => '1',
        ]);
        Room::create([
            'room_no' => '202',
            'floor_id' => '1',
            'roomtype_id' => '1',
        ]);
        Room::create([
            'room_no' => '203',
            'floor_id' => '1',
            'roomtype_id' => '1',
        ]);

        Room::create([
            'room_no' => '204',
            'floor_id' => '1',
            'roomtype_id' => '1',
        ]);

        Room::create([
            'room_no' => '301',
            'floor_id' => '2',
            'roomtype_id' => '2',
        ]);
        Room::create([
            'room_no' => '302',
            'floor_id' => '2',
            'roomtype_id' => '2',
        ]);
        Room::create([
            'room_no' => '303',
            'floor_id' => '2',
            'roomtype_id' => '2',
        ]);

        Room::create([
            'room_no' => '304',
            'floor_id' => '2',
            'roomtype_id' => '2',
        ]);

        Room::create([
            'room_no' => '401',
            'floor_id' => '3',
            'roomtype_id' => '3',
        ]);
        Room::create([
            'room_no' => '402',
            'floor_id' => '3',
            'roomtype_id' => '3',
        ]);
        Room::create([
            'room_no' => '403',
            'floor_id' => '3',
            'roomtype_id' => '3',
        ]);

        Room::create([
            'room_no' => '404',
            'floor_id' => '3',
            'roomtype_id' => '3',
        ]);

        Room::create([
            'room_no' => '501',
            'floor_id' => '4',
            'roomtype_id' => '4',
        ]);

        Room::create([
            'room_no' => '502',
            'floor_id' => '4',
            'roomtype_id' => '4',
        ]);

        Room::create([
            'room_no' => '503',
            'floor_id' => '4',
            'roomtype_id' => '4',
        ]);
        Room::create([
            'room_no' => '504',
            'floor_id' => '4',
            'roomtype_id' => '4',
        ]);

        Room::create([
            'room_no' => '601',
            'floor_id' => '5',
            'roomtype_id' => '5',
        ]);
        Room::create([
            'room_no' => '602',
            'floor_id' => '5',
            'roomtype_id' => '5',
        ]);
        Room::create([
            'room_no' => '603',
            'floor_id' => '5',
            'roomtype_id' => '5',
        ]);
        Room::create([
            'room_no' => '604',
            'floor_id' => '5',
            'roomtype_id' => '5',
        ]);
    }
}
