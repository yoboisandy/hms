<?php

namespace Database\Seeders;

use App\Models\Roomtype;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoomtypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Roomtype::create([
            'type_name' => 'Standard Room',
            'occupancy' => '2',
            'description' => 'This is the most basic and common type of hotel room. It usually includes essential amenities.',
            'image' => 'images/roomtypes/standard_room.jpg',
            'price' => '1000'
        ]);
        $data->amenities()->sync([2, 3, 1]);

        $data = Roomtype::create([
            'type_name' => 'Deluxe Room',
            'description' => 'The Deluxe Room is a tastefully designed and comfortable accommodation option, offering a serene and inviting ambiance for both leisure and business travelers. These rooms are known for their elegant decor, contemporary furnishings, and thoughtfully curated amenities, ensuring a delightful stay experience.',
            'occupancy' => '3',
            'image' => 'images/roomtypes/deluxe_room.jpg',
            'price' => '2500'
        ]);
        $data->amenities()->sync([1, 2, 3, 5]);

        $data = Roomtype::create([
            'type_name' => 'Family Room',
            'description' => 'A family room is designed to accommodate families or groups and may have multiple beds or separate sleeping areas to cater to more guests.',
            'occupancy' => '5',
            'image' => 'images/roomtypes/family_room.jpg',
            'price' => '2500'
        ]);
        $data->amenities()->sync([1, 2, 3, 5]);

        $data = Roomtype::create([
            'type_name' => 'Super Deluxe Room',
            'description' => "These are two or more rooms with a connecting door, allowing larger groups or families to stay together while maintaining some privacy.",
            'occupancy' => '3',
            'image' => 'images/roomtypes/super_deluxe_room.jpg',
            'price' => '3500'
        ]);
        $data->amenities()->sync([1, 2, 3, 4, 5, 6, 7]);

        $data = Roomtype::create([
            'type_name' => 'Connecting Rooms',
            'description' => "These are two or more rooms with a connecting door, allowing larger groups or families to stay together while maintaining some privacy.",
            'occupancy' => '8',
            'image' => 'images/roomtypes/connecting_rooms.jpg',
            'price' => '3500'
        ]);
        $data->amenities()->sync([1, 2, 3, 4, 5, 6, 7]);
    }
}
