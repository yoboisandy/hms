<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'code', 'discount_percentage', 'valid_from', 'valid_until', 'status', 'roomtype_id'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function roomtype()
    {
        return  $this->belongsTo(Roomtype::class);
    }
}
