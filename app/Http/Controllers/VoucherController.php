<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Roomtype;
use App\Models\User;
use App\Models\Voucher;
use App\Notifications\VoucherAddedNotification;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Retrieve all vouchers
        $vouchers = Voucher::withoutTrashed()->with('roomtype')->get();
        return response()->json($vouchers);
    }

    public function trashed()
    {
        // Retrieve deleted vouchers
        $vouchers = Voucher::onlyTrashed()->with('roomtype')->get();
        return response()->json($vouchers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate request data
        $data = $request->validate([
            'code' => 'required|unique:vouchers',
            'discount_percentage' => 'required|integer|min:1|max:100',
            'valid_from' => 'required|date',
            'valid_until' => 'required|date|after:valid_from',
            'roomtype_id' => [],
        ]);

        // Create a new voucher
        $voucher = Voucher::create($data);

        $booking_users = Book::withTrashed()->where('roomtype_id', $request->roomtype_id)->pluck('user_id')->toArray();

        $users = User::whereIn('id', $booking_users)->get();


        $roomType = Roomtype::find($request->roomtype_id);
        $bookingdetail = [
            "body" => "New discount voucher is available now for room: $roomType->type_name",
            "footer" => "We will reach back to you soon",
            "url" => url('http://localhost:3000/rooms/' . $roomType->id)
        ];

        foreach ($users as $user) {
            $user->notify(new VoucherAddedNotification($bookingdetail));
        }

        return response()->json(['message' => 'Voucher created sucessfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function show(Voucher $voucher)
    {
        return response()->json($voucher);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voucher $voucher)
    {
        $data = $request->validate([
            'status' => "",
        ]);

        if ($data['status'] == 'Expired') {
            $voucher->update($data);
            $voucher->delete();
        }

        return response()->json(['message' => 'Voucher updated sucessfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voucher $voucher)
    {
        // Delete a specific voucher by ID
        $vouch = Voucher::findOrFail($voucher);
        $vouch->delete();

        return response()->json(['message' => 'Voucher deleted successfully']);
    }
}
