<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Hall;
use App\Models\Room;
use App\Models\Customer;
use App\Models\Employee;
use App\Models\Roomtype;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CountController extends Controller
{
    public function countAll()
    {
        $count = [
            "department" => Department::count(),
            "employee" => Employee::count(),
            "room" => Room::count(),
            "roomtype" => Roomtype::count(),
            "hall" => Hall::count(),
            "customer" => Customer::count(),
            "checkin" => Book::where('status', 'Checked In')->count(),
            "income" => DB::table('books')->where('status', 'Checked Out')->sum('price'),
            "cancel" => DB::table('books')->where('status', 'Cancelled')->count(),
            "serve_order" => DB::table('orders')->where('status', 'Delivered')->count(),
            "pending_order" => DB::table('orders')->where('status', 'Pending')->count(),
            "orders" => DB::table('orders')->count(),
        ];
        return response()->json($count);
    }
}
