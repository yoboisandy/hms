<?php

namespace App\Http\Controllers;

use ErrorException;
use App\Models\Book;
use App\Models\Food;
use App\Models\Room;
use App\Models\User;
use App\Models\Order;
use App\Models\Voucher;
use App\Models\Roomtype;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Notifications\BookingConfirmed;
use App\Notifications\BookingRequestSent;
use Illuminate\Support\Facades\Validator;
use App\Notifications\RoomBookingCanceled;
use App\Notifications\BookingStatusChanged;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\ValidationException;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $room = Book::withTrashed()->with(['roomtype.rooms', 'user', 'room', 'review'])->ORDERBY('created_at', 'desc')->get();
        return response()->json($room);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $occupancy = Roomtype::findOrFail($request->roomtype_id)->occupancy;
        $now = Carbon::now()->format('Y-m-d');
        $request->validate([
            // 'room_req' => ['required', 'gte:1'],
            'start_date' => ['bail', 'required',  'required_with:end_date', 'after_or_equal:' . $now,  'date', 'before_or_equal:end_date'],
            'end_date' => ['bail', 'required', 'required_with:start_date', 'date', 'after_or_equal:start_date'],
            'roomtype_id' => ['required', 'exists:roomtypes,id'],
            'room_id' => ['required', 'exists:rooms,id'],
            'occupancy' => [],
        ]);
        $start_date = Carbon::parse($request->start_date);
        $end_date = Carbon::parse($request->end_date);
        $data = ['user_id' => Auth::user()->id, 'room_id' => $request->room_id, 'roomtype_id' => $request->roomtype_id,];
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        $a = Roomtype::select('price')->where('id', $data['roomtype_id'])
            ->get('price');
        $price = $a->sum('price');


        $data['price'] = $price * $end_date->diffInDays($start_date);


        if ($request->voucher == '') {
            $vouch = "empty";
        } else {
            $v = $request->voucher;
            // return $v;
            $vouch = Voucher::where('code', $v)->where('status', 'Enable')->count();
            $percent = Voucher::where('code', $v)->where('status', 'Enable')->sum('discount_percentage');
        }

        if ($vouch === 0) {
            $validator = Validator::make([], []);
            $validator->errors()->add('voucher', 'Invalid voucher code.');
            throw new ValidationException($validator);
        } elseif ($vouch === 'empty') {
            $data['voucher'] = "not_applied";
        } else {
            $data['voucher'] = 'applied';
            $discount_amount = ($data['price'] * $percent) / 100;
            // return $discount_amount;
            $data['price'] = $data['price'] - $discount_amount;
        }

        $booking_rooms = Book::where(function ($query) use ($start_date, $end_date) {
            $query->where('start_date', '<=', $end_date)
                ->where('end_date', '>=', $start_date);
        })
            ->pluck('room_id')
            ->toArray();

        $rooms = Room::whereNotIn('id', $booking_rooms)->get()->pluck('id')->toArray();

        if (in_array($data['room_id'], $rooms)) {
            $book = Book::create($data);
            $bookingdetail = [
                "body" => "Your Booking Request for Room No. " . $book->room->room_no .  " of type: " . $book->roomtype->type_name . " Has Been Sent Successfully",
                "footer" => "We will reach back to you soon",
                "url" => url('localhost:3000/bookings')
            ];
            $user = User::find(auth()->user()->id);
            $user->notify(new BookingRequestSent($bookingdetail));
            return response()->json(['message' => 'Room booked sucessfuly', $book]);
        } else {
            return response()->json(['error' => 'Not available that room for that day!!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book, $id)
    {
        $data = $request->validate([
            'room_id' => ['required', 'exists:rooms,id', 'unique:books,room_id',],
        ]);
        $data['status'] = 'confirmed';

        $book->where('id', $id)->update($data);

        return response()->json(['message' => 'Room allocated to that bookings!!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
    }
    public function calculate($id)
    {
        $price = Book::where('user_id', $id)->where('status', 'Checked In')->sum('price');
        // return $price;
        $food = Order::where('user_id', $id)->where('status', 'Delivered')->sum('price');
        // return $food;

        $user = User::find($id);

        $response = [
            "user" => $user,
            "room" => $price,
            "food" => $food,
            "subtotal" => $price + $food,
            "discount" => $discount = $user->points >= 2000 ? $user->points * 0.5 : 0,
            "total" => $price + $food - $discount
        ];

        return response()->json($response);
    }

    public function showBookingOfUser()
    {
        $bookings = Book::withTrashed()->orderBy('id', 'desc')->with(['roomtype', 'room', 'review'])->where('user_id', auth()->user()->id)->get();
        return response()->json($bookings);
    }

    public function changeBookingStatus(Request $request, Book $book, Order $order)
    {
        $user = User::find($book->user_id);
        $book->update([
            'status' => $request->status
        ]);
        if ($book->status == "Confirmed") {
            $user->notify(new BookingConfirmed($book));
        }

        if ($book->status === "Canceled") {
            $user->notify(new RoomBookingCanceled($book));
        }

        if ($book->status == "Checked Out") {
            if ($user->points >= 2000) {
                $user->points = 0;
                $user->save();
            }
            Order::where('user_id', $user->id)->update(["status" => "Paid"]);

            $bookedDays = Carbon::parse($book->start_date)->diffInDays($book->end_date);
            $point = $bookedDays * 100;
            $user->increment('points', $point);
            $book->delete();
            // return $book;
        }


        return response()->json([
            'message' => 'Booking Status Updated',
        ]);
    }
    public function assignRoom(Request $request, Book $book)
    {
        $booked_rooms = $book->whereBetween('start_date', [$book->start_date, $book->end_date])
            ->orWhereBetween('end_date', [$book->start_date, $book->end_date])
            ->pluck('room_id');

        // return $booked_rooms;

        if ($booked_rooms->contains($request->room_id)) {
            return response()->json([
                "message" => "Room Already assigned"
            ]);
        }
        $book->update([
            'room_id' => $request->room_id
        ]);
        return response()->json([
            'message' => 'Room Assigned Successfully',
        ]);
    }

    // public function student_search(Request $request)
    // {
    //     $name = $request->firstName;

    //     if (!empty($name)) {
    //         $student = Student::query()
    //             ->where('first_name', 'LIKE', '%' . $name . '%')
    //             ->with('guardian', 'grade')->paginate(15);
    //         return response()->json([
    //             'data' => $student,
    //             'success' => true,
    //         ], 200);
    //     } else {
    //         return response()->json([
    //             'success' => false,
    //         ], 200);
    //     }
    // }
}
